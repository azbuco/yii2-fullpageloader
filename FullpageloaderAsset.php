<?php

namespace azbuco\fullpageloader;

use yii\web\AssetBundle;

class FullpageloaderAsset extends AssetBundle {

    public $sourcePath = '@azbuco/fullpageloader/assets';
    public $css = [
        'css/fullpageloader.css',
    ];
    public $js = [
        'js/fullpageloader.js',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];

}
