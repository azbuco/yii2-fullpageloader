<?php

namespace azbuco\fullpageloader;

use yii\base\Widget;
use yii\helpers\Json;

/**
 * Class Fullpageloader
 * @package azbuco\fullpageloader
 */
class Fullpageloader extends Widget
{
    
    public $type = 'bars';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
}

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerBundle();
        $this->registerClientScript();

        return $this->render('main', [
            'type' => $this->type,
        ]);
    }

    /**
     * Registers plugin and the related events
     */
    protected function registerBundle()
    {
        $view = $this->getView();
        FullpageloaderAsset::register($view);
    }

    /**
     * Register JS
     */
    protected function registerClientScript()
    {
        $id = $this->getId();
        $js = "";
        $this->getView()->registerJs($js);
    }
}
