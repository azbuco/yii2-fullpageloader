<?php
    /* @var $this \yii\web\View */
    /* @var $type string Type of animation */
?>
<div class="fullpageloader-overlay" style="display: none;">
    <div class="fullpageloader-animation">
        <?= $this->render('_' . $type); ?>
    </div>
    <div class="fullpageloader-text">Loading...</div>
</div>
