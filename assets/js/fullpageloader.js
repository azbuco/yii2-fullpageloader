var fullpageloader = (function () {
    var api = {};

    api.overlayEl = $('.fullpageloader-overlay');
    api.textEl = $('.fullpageloader-text');
    
    api.requestId = 1;
    api.activeRequests = [];

    api.show = function (text) {
        api.trigger('fpl:show');

        if (typeof text === 'undefined') {
            api.text('');
        } else {
            api.text(text);
        }
        
        if (api.overlayEl.is(':visible')) {
            return;
        }

        api.overlayEl.css('opacity', 0);
        api.overlayEl.show();
        api.overlayEl.stop().animate({
            opacity: 1
        }, 500, function () {
            api.trigger('fpl:shown');
        });
    };

    api.hide = function () {
        api.trigger('fpl:hide');
        
        if (!api.overlayEl.is(':visible')) {
            return;
        }
        
        api.overlayEl.css('transition', 'opacity 0.1s');
        api.overlayEl.stop().animate({
            opacity: 0
        }, 200, function () {
            api.overlayEl.hide();
            api.trigger('fpl:hidden');
        });
    };

    api.text = function (text) {
        api.textEl.html(text);
    };

    api.trigger = function (type, params) {
        if (!params) {
            params = {};
        }
        var e = jQuery.Event(params);
        e.type = type;

        api.overlayEl.trigger(e);
    };
    
    api.refresh = function() {
        if (api.activeRequests.length === 0) {
            api.hide();
        } else {
            api.show();
        }
    };

//    var init = function () {
//        $(document).ajaxSend(function (event, request, settings) {
//            debugger;
//            
//            request.fullpageloaderRequestId = api.requestId++;
//            api.activeRequests.push(request);
//            
//            api.refresh();
//        });
//        
//        $(document).ajaxComplete(function (event, request, settings) {
//            debugger;
//            if (request.fullpageloaderRequestId) {
//                api.activeRequests = api.activeRequests.filter(function(el) {
//                    return el.fullpageloaderRequestId !== request.fullpageloaderRequestId;
//                });
//            }
//            
//            api.refresh();
//        });
//    };
//
//    init();

    return api;
})();


